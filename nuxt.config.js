const axios = require('axios')

export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'server',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: 'Uwe Spannagel - %s',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.svg' }
    ]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [{ src: '~/assets/main.css', lang: 'css' }, { src: 'vue-slick-carousel/dist/vue-slick-carousel.css' }],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [{ src: './plugins/vue-slick-carousel.js' }],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    'nuxt-gsap-module',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/i18n',
    '@nuxt/content',
    '@nuxtjs/markdownit',
    '@nuxt/image',
    '@nuxtjs/sitemap'
  ],




  sitemap: {
    hostname: 'https://uwespan-preview.vercel.app',
    routes: async () => {
      const { data } = await axios.get(process.env.APP_BASE_URL + 'api/collections/get/projekte?populate=1&lang=')
      const { sonstige } = await axios.get(process.env.APP_BASE_URL + 'api/collections/get/sonstige')
      const de = data.entries.map((entry) => `/projekte/${entry.title_slug}`);
      const en = data.entries.map((entry) => `/en/projekte/${entry.title_slug}`)
      const all = de.concat(en);
      return all;
    }
  },

  i18n: {
    locales: ['de', 'en'],
    defaultLocale: 'de'
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: process.env.APP_BASE_URL // Used as fallback if no runtime config is provided
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
		loaders: {
			css: {
				modules: false,
			},
		},
	},

  image: {
    domains: ['https://cms.uwespannagel.com/'],
    alias: {
      cms: 'https://cms.uwespannagel.com/'
   }
  },
  markdownit: {
    preset: 'default',
    linkify: true,
    breaks: true
  },

  gsap: {
    extraPlugins: {
      scrollTrigger: true
    }
  }
}
