module.exports = {
  mode: 'jit',
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    fontFamily: {
      'roobert': ['roobert_trialmedium', 'sans-serif']
    },
    colors: {
      base: '#242424',
      'base-light': '#2C2C2C',
      'base-lighter': '#3A3A3A',
      'base-lightest': '#494949',
      'base-dark': '#1A1919',
      'base-darker': '#141414',
      light: '#EDEDED',
      red: 'tomato',
    },
    extend: {
      fontSize: {
        dynamicMiddle: "clamp(1rem, 2.5vw, 4rem)",
        dynamicLarge: "clamp(1.5rem, 3.3vw, 5rem)",
        dynamicMobile: "clamp(2rem, 3.3vw, 5rem)",
      },
    },
  },
  variants: {},
  plugins: [],
}
